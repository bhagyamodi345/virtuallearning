import Land from "./components/landingpage/Land";
import Temp from "./components/model/LoginModal";

 

function App() {
  return (
    <div>
      <Land />
      <Temp />
    </div>
  );
}

export default App;
