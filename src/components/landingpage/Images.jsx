import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import './land.css'

function Images() {
  return (
    <div> <Container fluid>
    <Row>
      <Col>
      <h1 className='myhead'>Welcome to Programmers Hub</h1>
      <p className='mypara'>Experience coding with Real-Time Projects, Corporate training programs are based on related to the essential training employees need to operate certain equipment.
</p>
<div className="mybutton">
<Button variant="outline-primary">Primary</Button>{' '}
<Button variant="outline-warning">Warning</Button>{' '}
<Button variant="outline-info">Info</Button>{' '}

</div>
      </Col>
      
      <Col xs={6} md={4} style={{ margin: '200px 0 0 200px' }}
>
        <Image src="https://bitstreamio.com/Admin/assets/img/hero-img.png" rounded />
      </Col>
      
    </Row>
   
  </Container></div>
  )
}

export default Images